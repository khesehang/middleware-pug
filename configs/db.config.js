//  HERE WE GONNA IMPORT ALL THE THINGS NEEDED FOR USING MONGO SO NOT TO IMPORT MANNUALLY IN EVERY FILE WHILE WE CHANGE FILE PATH
const mongodb = require('mongodb');
const MONGO_CLIENT = mongodb.MongoClient;
const CONXN_URL = 'mongodb://localhost:27017';
const DB_NAME = 'group36db';
const oid = mongodb.ObjectId;

module.exports = {
    MONGO_CLIENT : MONGO_CLIENT,
    CONXN_URL : CONXN_URL,
    DB_NAME : DB_NAME,
    OID : oid
}