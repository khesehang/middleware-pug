
const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    // db modelling
    firstName:String,
    lastName:String,
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
        trim: true
    },
    email:{
        type:String,
        unique:true
    },
    status:{
        type:String,
        enum:['online','offline']
    },
    contact:{
        mobile:{
            type:Number,
            rquired:true
        },
        home:Number,
        alternateNumber:Number
    },
    image:String,
    dob:Date,
    gender:{
        type:String,
        enum: ['male','female', 'others']
    },
    password:{
        type:String,
        required:true
    },
    role:{
        type:Number, // 1 for admin, 2 for normal user, 3 visitor
        default: 2
    }

})

const UserModel = mongoose.model('user',UserSchema)
module.exports = UserModel;

// {enum} is only option like you give option but cannot choose except the option