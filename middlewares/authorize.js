const JWT = require('jsonwebtoken');
const config = require('./../configs');

module.exports = function (req, res, next) {
    let token;
    if (req.headers['authorization'])
        token = req.headers['authorization']
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.headers['token'])
        token = req.headers['token']
    if (req.query.token)
        token = req.query.token

    if (!token) {
        return next({
            msg: 'Authentication Failed, Token Not Provided',
            status: 401
        })
    }
    // if token exist proceed with verification
    JWT.verify(token, config.JWT_SECRET, function (err, decoded) {
        if (err) {
            return next(err)
        }
        console.log('token verification successfull>>', decoded)
        next();
    })
}