const path = require('path')
const multer = require('multer')

module.exports = function (filterType) { 
    function imageFilter(req, file, cb) {
        const mimeType = file.mimetype.split('/')
        console.log('mimetype is', mimeType)
        if (mimeType[0] === 'image') {
            cb(null, true);
        } else {
            req.fileTypeError = true;
            cb(null, false);
        }
    }

    const myStorage = multer.diskStorage({
        filename: function (req, file, cb) {
            cb(null, Date.now() + '-' + file.originalname)
        },
        destination: function (req, file, cb) {
            cb(null, path.join(process.cwd(), 'uploads/images'))
        }
    })

    const upload = multer({
        storage: myStorage,
        fileFilter: imageFilter
    })
    return upload;
}