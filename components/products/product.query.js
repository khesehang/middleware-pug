const ProductModel = require('./product.model');

// db query

function map_product_req(product, productData) {
    if (productData.name)
        product.name = productData.name;
    if (productData.description)
        product.description = productData.description;
    if (productData.category)
        product.category = productData.category;
    if (productData.brand)
        product.brand = productData.brand;
    if (productData.size)
        product.size = productData.size;
    if (productData.images)
        product.images = productData.images;
    if (productData.price)
        product.price = productData.price;
    if (productData.quantity)
        product.quantity = productData.quantity;
    if (productData.status)
        product.status = productData.status;
    if (productData.modelNo)
        product.modelNo = productData.modelNo;
    if (productData.vendor)
        product.vendor = productData.vendor;
    if (productData.warrentyStatus)
        product.warrentyStatus = productData.warrentyStatus;
    if (productData.color)
        product.color = productData.color;
    if (productData.isReturnEligible)
        product.isReturnEligible = productData.isReturnEligible;
    if (productData.menuDate)
        product.menuDate = productData.menuDate;
    if (productData.salesDate)
        product.salesDate = productData.salesDate;
    if (productData.purchasedDate)
        product.purchasedDate = productData.purchasedDate;
    if (productData.returnedDate)
        product.returnedDate = productData.returnedDate;
    if (productData.offers)
        product.offers = typeof (productData.offers) === 'string' ?
        productData.offers.splite(',') :
        productData.offers;
    if (productData.tags)
        product.tags = typeof (productData.tags) === 'string' ?
        productData.tags.split(',') :
        productData.tags;
    if (!productData.discount)
        product.discount = {};
    if (productData.discountItem) // x-www-form-url encoded ma boolean value string huncha
        product.discount.discountItem = productData.discountItem;
    if (productData.discountype)
        product.discount.discountype = productData.discountype;
    if (productData.discountValue)
        product.discount.discountValue = productData.discountValue;


}



function find(condition) {
    // return new Promise(function (resolve, reject) {
    //     ProductModel
    //         .find(condition)
    //         .exec()
    //         .then(function (response) {
    //             resolve(response);
    //         })
    //         .catch(function (err) {
    //             reject(err);
    //         })
    // })
    return ProductModel
        .find(condition)
        .sort({
            _id: -1
        })
        // .populate
        .exec();

}

function insert(data) {
    const newProduct = ProductModel({});
    map_product_req(newProduct, data);
    return newProduct
        .save()
}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 400
                })
            }
            map_product_req(product, data)
            product.save(function (err, updated) {
                if (err) {
                    return reject(err);
                }
                resolve(updated)
            })
        })
    })
}

function remove(id) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            product.remove(function (err, removed) {
                if (err) {
                    return reject(err)
                }
                resolve(removed)
            })
        })
    })
}

function addReview(productId, reviewData) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(productId, function (err, product) {
            if (err) {
                return reject(err)
            }
            if (!product) {
                return reject({
                    msg: "Product Not Found",
                    status: 404
                })
            }
            product.reviews.push(reviewData);
            product.save(function (err, updated) {
                if (err) {
                    return reject(err)
                }
                resolve(updated);
            })
        })
    })
}

module.exports = {
    find,
    insert,
    update,
    remove,
    addReview
}