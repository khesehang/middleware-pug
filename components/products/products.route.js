const productCtrl = require('./products.controller')
const router = require('express').Router()
const authorize = require('./../../middlewares/authorize')

router.route('/')
    .get(authorize,productCtrl.get)
    .post(authorize,productCtrl.post)

router.route('/search')
    .get(productCtrl.search)
    .post(productCtrl.search)

router.route('/add_review/:product_id')
    .post(authorize,productCtrl.addReview)
        
router.route('/:id')
    .get(authorize,productCtrl.getById)
    .put(authorize,productCtrl.update)
    .delete(authorize,productCtrl.remove);

module.exports = router;