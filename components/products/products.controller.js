const ProductModel = require('./product.model')
const ProductQuery = require('./product.query')

// application logic

function post(req, res, next) {
    const data = req.body;
    // TODO add require value in data
    ProductQuery
        .insert(data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function get(req, res, next) {
    var condition = {}
    // TODO prepare condition
    ProductQuery
        .find(condition) // own find (not of mongoose)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err);
        })
}

function update(req, res, next) {
    const data = req.body

    ProductQuery
        .update(req.params.id, data)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            next(err)
        })
}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (response) {
            res.json(response)
        })
        .catch(function (err) {
            return next(err)
        })
}

function search(req, res, next) {
    const SearchCondition = {}
    ProductQuery
        .find(SearchCondition)
        .then(function (result) {
            res.json(result)
        })
        .catch(function (err) {
            return next(err)
        })

}

function addReview(req, res, next) {
    if (!(req.body.reviewMessage && req.body.reviewPoint)) {
        return next({
            msg: "Missing Required Fields",
            status: 404
        })
    }
    const productId = req.params.product_id;
    const reviewData = {
        message: req.body.reviewMessage,
        point: req.body.reviewPoint
    }
    // add reviewer
    ProductQuery
        .addReview(productId, reviewData)
        .then(function (respone) {
            res.json(respone)
        })
        .catch(function (err) {
            return next(err)
        })
}

function getById(req, res, next) {
    const condition = {
        _id: req.params.id
    }
    ProductQuery
        .find(condition)
        .then(function (result) {
            res.json(result)
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    post: post,
    get: get,
    update: update,
    remove: remove,
    search: search,
    getById: getById,
    addReview: addReview
}