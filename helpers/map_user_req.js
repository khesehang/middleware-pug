module.exports = function(user,userData) {
     if(userData.firstName)
        user.firstName = userData.firstName;
    if(userData.lastName)
        user.lastName = userData.lastName;
    if(userData.username)
        user.username = userData.username;
    if(userData.password)
        user.password = userData.password;
    if(userData.gender)
        user.gender = userData.gender;
    
    // deal with object
    if(!user.contact)
        user.contact = {};
    if(userData.mobileNumber)
        user.contact.mobile = userData.mobileNumber
    if(userData.home)
        user.home = userData.homeNumber
    if(userData.alternateNumber)
        user.contact.alternateNumber = userData.alternateNumber

    if(userData.image)
        user.image = userData.image; 
    
    return user;
}