const mongoose = require('mongoose');
const db_config = require('./configs/db.config'); 

// mongoose.set('useNewUrlParser',true);
// mongoose.set('useCreateIndex',true);

// mongodb://localhost:27017/db_name;
mongoose.connect(db_config.CONXN_URL+'/'+db_config.DB_NAME, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex:true
}, function(err,done) {
    if(err){
        console.log("db connection failed",err)
    } else {
        console.log("db connection open")
    }
})

// if not {connect} than {createConnection}