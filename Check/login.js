module.exports = (req,res,next)=>{
    if(req.query.role === 'logged'){
    next();
    }else{
        next({
            mess:'you are not logged in',
            ans:'First login your account'
        })
    }
}