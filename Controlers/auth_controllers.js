const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req');
const path = require('path');
const multer = require('multer');
const passwordHash = require('password-hash');
const JWT = require('jsonwebtoken')
const config = require('./../configs');
const uploader = require('./../middlewares/uploader')('image')

function generateToken(data) {
    let token = JWT.sign({
        _id: data._id,
    }, config.JWT_SECRET);
    return token;
}

// this is to unlink
router.get('/remove/:filename', function (req, res, next) {
    require('fs').unlink(path.join(process.cwd(), 'uploads/images/' + req.params.filename), function (err, removed) {
        if (err) {
            return next(err);
        }
        res.json({
            msg: 'removed',
            val: removed
        })
    })
})
router.get('/', function (req, res, next) {
    UserModel
        .find()
        .sort({
            _id: -1
        })
        .then(user => {
            res.json(user)
        })
        .catch(err => {
            return next(err)
        })
})
router.post('/register', uploader.single('image'), function (req, res, next) {
    console.log('req.body>>', req.body)
    console.log('req.file>>', req.file)
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Formate",
            status: 404
        })
    }
    const newUser = new UserModel({})

    const data = req.body

    if (data.file) {
        data.image = req.file.filename;
    }
    const newMappedUser = MAP_USER_REQ(newUser, data)
    newMappedUser.password = passwordHash.generate(req.body.password)
    // mongoose method
    newMappedUser
        .save()
        .then(saved => {
            res.json(saved)
        })
        .catch(err => {
            return next(err)
        })
})

router.post('/login', function (req, res, next) {
    console.log('req.body>>', req.body)
    UserModel
        .findOne({
            username: req.body.username
        })
        .then(function (user) {
            // console.log('user is',user)
            if (!user) {
                return next({
                    mess: "username invalid",
                    status: 404
                })
            }
            const isMatched = passwordHash.verify(req.body.password, user.password)
            if (!isMatched) {
                return next({
                    msg: "Invalid password",
                    status: 426
                })
            }
            if (!user.status === 'online') {
                return next({
                    mess: "sorry your account is expired",
                    status: 403
                })
            }
            // generate webtoken
            token = generateToken(user);
            res.json({
                user: user,
                token: token
            })
        })
        .catch(function (err) {
            return next(err);
        })
    // console.log('username is>>',req.body.userName)
})






















































// this all are another way
//{ const mon{godb = require('mongodb')
// const MONGO_CLIENT = mongodb.MongoClient;
// const CONXN_URL = 'mongodb://localhost:27017'
// const DB_NAME = 'group36db'}}

// router.route('/')
//     .get((req, res, next) => {
//         res.render('index.pug');
//     })
// // .post((req,res,next)=>{
// //     req.status(200).body(data)
// // })


// router.post('/login', function(req,res,next) {
//     console.log('req.body from auth login>>', req.body)

//     MONGO_CLIENT.connect(CONXN_URL,{useUnifiedTopology:true},function(err,client){
//         if(err){
//             console.log('db connect in login failed')
//             return next(err);
//         }
//         const db = client.db(DB_NAME)
//         db
//             .collection('users')
//             // .find({username:req.body.username})
//             .find()
//             .toArray(function(err,users) {
//                 if(err){
//                     return next(err);
//                 }
//                 res.json(users)
//             })
//     })
// })


// router.post('/register', function(req,res,next) {
//         // requesting body in data variable
//         console.log('req.body is',req.body)
//         const data = req.body;
//         // db_connection
//     MONGO_CLIENT.connect(CONXN_URL,{useUnifiedTopology: true},function(err,client){
//             if(err){
//                 console.log('monogodb connection failed');
//                 return next(err);
//             }
//             console.log('mongodb connection successfull');
//             // db operation
//             // select db
//             const selected_db = client.db(DB_NAME);
//             // query
//             selected_db
//                 .collection('users')
//                 .insertOne({name:"khesehang"})
//                 .then(function(result){
//                     res.json(result)
//                 })
//                 .catch(function(err){
//                     next(err) 
//                 })
//         })
//     })

// router.route('/login')
//     .get((req, res, next) => {
//         res.json({
//             mess: 'from empty auth login',
//             photo: 'img.png',
//             account: '@samsohang'
//         })
//     })// this is another way
// this dynamic middleware should always be at last
// router.route('/:id')
//     .get((req, res, next) => {
//         res.json({
//             mess: 'from empty auth dynamic',
//             mess: 'from dynamic middleware',
//             params: req.params
//         })
//     })


module.exports = router;