const router = require('express').Router();
const UserModel = require('./../models/user.model')
// const UserModel = require('./../models/user.model.js')
const MAP_USER_REQ = require('./../helpers/map_user_req')

router.route('/')
    .get(function(req,res,next) {
        // get all the users
        UserModel
        // .find({
            // password:0,This says exclusion{0}
            // usernaem:1 This says inclusion {1}
            // _id:0 As id is always given by default so we should do only exclusion
            // As given above we cannot use inclusion and exclusion both on same method but we can do with {_id and others} because {_id  is by default so not to need it we can do only with it}
        // })
        // .find({},{This is projection where we use inclusion and exclusion})
            .find({})
            .sort({
                _id:-1
            })
            .exec( function(err,users) {
                if(err){
                    return next(err);
                }
                res.json(users)
            })
            // exec is used in only {find} method to sort and execute for more easily
            // console.log("req.body",req.body.name)

    })
router.route('/register')
    .post( function(req,res,next) {
        const newUser = new UserModel({})

        const postUser = MAP_USER_REQ(newUser,req.body)

        newUser
            .save()
            .then( function(saved) {
                if(!saved) {
                    return next({
                        msg:"unable to save",
                        status:404
                    })
                }
                res.json(saved)
            })
            .catch( function(err) {
                return next({
                    msg:"error registering",
                    status: 404
                })
            })

        // postUser.save( function(err,saved) {
        //     if(err) {
        //         return next(err);
        //     }
        //     res.json(saved);
        // })
    })


router.route('/:userId')
    .get( function(req,res,next) {
        let id = req.params.userId;
        // find by ID
        UserModel
            // .find({ _id: id })
            // .findOne({ _id: id })As id super unique why don't we use {findOne({})} method as it is display only one value
            // But there is more interesting
            .findById(id)
            .then( function(user) {
                if (!user) {
                    return next({
                        msg:"user by id not found",
                        status: 404
                    })
                }
                res.json(user);
            })
            .catch( function(err) {
                return next(err);
            })
    })
    .put( function(req,res,next) {
        UserModel.findById(req.params.userId, function(err,user) {
            if(err) {
                return next(err)
            }
            if(!user) {
                return next({
                    msg:"Didn't found user id to update",
                    status: 404
                })
            }
            // if user exist proceed with update
            // here {user} is mongoose object
            const updatedMapUser = MAP_USER_REQ(user,req.body)
            console.log(updatedMapUser)
            // once object is updated with provided value

            updatedMapUser.save( function( err, updated) {
                if(err) {
                    return next(err);
                }
                res.json(updated);
            })
        })            
    })
    .delete( function(req,res,next) {
        UserModel.findById(req.params.userId, function(err,user) {
            if(err) {
                return next(err);
            }
            if(!user) {
                return next({
                    msg:"user not found",
                    status: 404
                })
            }
            user.remove( function(err,removed) {
                if(err) {
                    return next(err);
                }
                res.json(removed)
            })
        })
    })

















module.exports = router;
































































// const dbConfig = require('./../configs/db.config');

// function db_connection(cb) {
//     dbConfig.MONGO_CLIENT.connect(dbConfig.CONXN_URL,{useUnifiedTopology:true},function(err,client){
//         if(err){
//             cb(err);
//         }else{
//             const db = client.db(dbConfig.DB_NAME)
//             cb(null,db)
//         }
//     })
// }

// // sub route

// router.route('/')
//     .get(function(req,res,next){
//         db_connection(function(err,db){
//             if(err){
//                 console.log('failed connecting mongodb in user')
//                 next(err);
//             }
//             db
//                 .collection('users')
//                 .find()
//                 .toArray( function(err,users){
//                     console.log('user is ',users)
//                     if(err){
//                         return next(err)
//                     }
//                     res.json(users)
//                 })
//         })
//     })

// router.route('/:userid')
//     .get(function(req,res,next){
//         let id = req.params.userId;

//         db_connection(function(err, db) {
//             if(err){
//                 return next(err);
//             }
//             db
//                 .collection('users')
//                 .find({
//                     _id: new dbConfig.OID(id)
//                 })
//                 toArray(function (err, user) {
//                     if(err) {
//                         return next(err);
//                     }
//                     if(user[0]) {
//                         res.json(user[0])
//                     }else {
//                         next({
//                             msg:'User not found', 
//                             status: 404
//                         })
//                     }
//                 })
//         })
//     })
//     .put( function(req,res,next){
//         let user_id = new dbConfig.OID(req.params.userId)

//         db_connection(function(err,db){
//             if(err){
//                 return next(err);
//             }
//             db.collection('users').update(
//                 {_id:user_id},
//                 {$set:req.body},
//                 {upsert: true},
//                 function(err, update){
//                     if(err){
//                         return next(err)
//                     }
//                     res.json(update)
//                 }
//             )
//         })
//     })
//     .delete(function(req,res,next){
//         db_connection(function(err,db){
//             if(err){
//                 return next(err);
//             }
//             db
//                 .collection('user')
//                 .remove(new dbConfig.OID(req.params.userId))
//                 .then(function(data){
//                     res.json(data);
//                 })
//                 .catch(function(err){
//                     return next(err); tart
//                 })
//         })
//     })



































// router.get('/',function(req,res,next){
//     res.render('login.pug',{
//         title:'Login form from pug',
//         h2:'Login',
//         p:'Please login to continue'
//     });
// })

// router.route('/analystics')
// .get((req,res,next)=>{
//     res.json({
//         mess:"this is from user",
//         user:'i am user',
//         acc:'this is my account'
//     })
// })
// .post((req,res,next)=>{
//     res.json({
//         mess:"this is from user",
//         user:'i am user',
//         acc:'this is my account'
//     })
// })
// .put((req,res,next)=>{
//     res.json({
//         mess:"this is from user",
//         user:'i am user',
//         acc:'this is my account'
//     })
// })
// .delete((req,res,next)=>{
//     res.json({
//         mess:"this is from user",
//         user:'i am user',
//         acc:'this is my account'
//     })
// })

// router.route('/account')
// .get((req,res,next)=>{
//     res.status(200).json({
//         mess:"This is my account",
//         admin:"I am here admin",
//         access:"This is my account so i can use this account"
//     })
// })
// .post((req,res,next)=>{
//     res.status(200).json({
//         mess:"This is my account",
//         admin:"I am here admin",
//         access:"This is my account so i can use this account"
//     })
// })
// .put((req,res,next)=>{
//     res.status(200).json({
//         mess:"This is my account",
//         admin:"I am here admin",
//         access:"This is my account so i can use this account"
//     })
// })
// .delete((req,res,next)=>{
//     res.status(200).json({
//         mess:"This is my account",
//         admin:"I am here admin",
//         access:"This is my account so i can use this account"
//     })
// })

// module.exports = router;