// importing modules
const express = require('express');
const colors = require('colors');
const morgan = require('morgan');
const path = require('path');
const config = require('./configs/index')


require('./db_initilize.js'); // ===> Part of index.js file run 

// api route imprting
const api_route = require('./api.route')

const app = express();
app.use(express.urlencoded({
  extended: true
}));

app.use(express.json());

// this is required for middleware use
app.use(morgan('dev'));

app.use('/api',api_route)

app.use(function (error, req, res, next) {
  res.json({
    msg: error.msg || error,
    status: error.status || 400
  })
})


app.listen(config.PORT, console.log(`server running on port:${config.PORT}`.yellow.bold))








































































// app.use('/file', express.static(path.join(__dirname, 'Uploads')));
// // view engine setup also called express front end
// pug = require('pug');
// app.set('view engine', pug);
// app.set('views', path.join(__dirname, 'views'))
// inbuildts middleware for static middleware
// app.use(express.static('uploads')); // internal server (express as an independent application)// but not reccomended

// console.log("checking ",check)
// console.log("__dirname is",__dirname)
// console.log('root directry path',process.cwd())

// This is to check or handle all the error
// app.get('/',(req,res,next)=>{
//     res.render('index.pug',{
//         title:'pug first learning'
//     })
// })