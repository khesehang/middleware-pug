const router = require('express').Router();

// importing middlewares
const authorize = require('./middlewares/authorize')

// impoting files
const auth_controll = require('./Controlers/auth_controllers')
const user_controll = require('./Controlers/user_controllers')
const Products = require('./components/products/products.route')

// file path
router.use('/auth', auth_controll)
router.use('/user',authorize,  user_controll)
router.use('/product',Products)

module.exports = router;